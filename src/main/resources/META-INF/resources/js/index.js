'use strict';

import JSXComponent from 'metal-jsx';

class HelloWorld extends JSXComponent {
	render() {
		return (
			<div class="todo-app">
				<ul>
					Hola a todos
				</ul>
			</div>
		);
	}
}

export default function(elementId) {
	new HelloWorld({ element: '#'+elementId });
}